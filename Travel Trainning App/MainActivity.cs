﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.Design.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using SupportFragment = Android.Support.V4.App.Fragment;
using SupportActionBar = Android.Support.V7.App.ActionBar;
using System;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Views;
using Android.Content;
using System.Collections.Generic;

namespace Travel_Trainning_App
{
    [Activity(Theme="@style/MyTheme" ,Label = "@string/app_name", MainLauncher = true)]

    public class MainActivity : AppCompatActivity, IOnMapReadyCallback
    {
        private Android.Support.V4.Widget.DrawerLayout drawerLayout;
        private NavigationView navView;
        private SupportFragment mCurrentfrag;
        private MyTrip mMytrip;
        private Contact mContact;

        //private Stack<SupportFragment> mStackfragment;

        public void OnMapReady(GoogleMap googleMap)
        {
            
        }

        protected override void OnCreate(Bundle savedInstanceState)
        { 
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            var mapFragment = (MapFragment)FragmentManager.FindFragmentById(Resource.Id.map);
            mapFragment.GetMapAsync(this);

            SupportActionBar ab = SupportActionBar;
            ab.SetHomeAsUpIndicator(Resource.Drawable.abc_ic_menu_copy_mtrl_am_alpha);
            ab.SetDisplayHomeAsUpEnabled(true);

            drawerLayout = FindViewById<Android.Support.V4.Widget.DrawerLayout>(Resource.Id.drawer_layout);
            navView = FindViewById<NavigationView>(Resource.Id.nav_view);

            mMytrip = new MyTrip();
            mContact = new Contact();
            
            var trans = SupportFragmentManager.BeginTransaction();
            trans.Add(Resource.Id.fragmentcontainer, mContact, "Contact");
            trans.Hide(mContact);
            trans.Add(Resource.Id.fragmentcontainer, mMytrip, "MyTrip");
            trans.Commit();
            //mCurrentfrag = mMap;
            void showFragment(SupportFragment fragment)
            {
                trans = SupportFragmentManager.BeginTransaction();
                trans.Hide(mCurrentfrag);
                trans.Show(fragment);
                trans.AddToBackStack(null);
                trans.Commit();
                mCurrentfrag = fragment;
            }

            if (navView != null)
            {
                SetUpDrawerContent(navView);
            }

            navView.NavigationItemSelected += (sender, e) =>
            {
                e.MenuItem.SetChecked(true);

                switch (e.MenuItem.ItemId)
                {
                    case Resource.Id.nav_main:
                        
                        break;
                    case Resource.Id.nav_mytrip:
                        showFragment(mMytrip);
                        
                        break;
                        
                    case Resource.Id.nav_share:
                        
                        break;
                    case Resource.Id.nav_contact:
                        showFragment(mContact);
                        break;
                    case Resource.Id.nav_info:
                        //intent = new Intent(this, typeof(Info));
                        //StartActivity(intent);
                        break;
                }
                
                drawerLayout.CloseDrawers();
            };

        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            //switch (item.ItemId)
            //{
            //    case Resource.Id.home:
                    drawerLayout.OpenDrawer((int)GravityFlags.Left);
            //        return true;
            //    default:
            //        return base.OnOptionsItemSelected(item);
            //}
            return base.OnOptionsItemSelected(item);
        }
        private void SetUpDrawerContent(NavigationView navView)
        {
            navView.NavigationItemSelected += (object sender, NavigationView.NavigationItemSelectedEventArgs e) =>
            {
                e.MenuItem.SetChecked(true);
                drawerLayout.CloseDrawers();
            };
        }
    }
}